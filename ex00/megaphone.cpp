/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   megaphone.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/05 14:11:19 by aeddi             #+#    #+#             */
/*   Updated: 2015/04/06 10:15:18 by aeddi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>

int main (int ac, char **av)
{
	int	i, i2;

	if (ac == 1)
		std::cout << "* LOUD AND UNBEARABLE FEEDBACK NOISE *" << std::endl;
	else
	{
		for (i = 1; i < ac; i++)
		{
			for (i2 = 0; av[i][i2] != '\0'; i2++)
			{
				if (av[i][i2] > 96 && av[i][i2] < 123)
					std::cout << char(av[i][i2] - 32);
				else
					std::cout << char(av[i][i2]);
			}
		}
		std::cout << std::endl;
	}
	return 0;
}
