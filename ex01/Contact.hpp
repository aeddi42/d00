/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Contact.hpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/05 15:14:11 by aeddi             #+#    #+#             */
/*   Updated: 2015/04/06 14:09:29 by aeddi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CONTACT_HPP
# define CONTACT_HPP

# include <iostream>

class Contact {

public:

	Contact(void);
	bool init(void);
	void display_short(int index);
	void display_full(void);

private:

	std::string	first_n;
	std::string	last_n;
	std::string	nick;
	std::string	login;
	std::string	post;
	std::string	email;
	std::string	phone;
	std::string	birth;
	std::string	meal;
	std::string	underw;
	std::string secret;
	bool eof_control;

	void getprompt(std::string prompt, std::string& field);

};

#endif /* !CONTACT_HPP */
