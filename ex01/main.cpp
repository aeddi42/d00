/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/05 15:14:36 by aeddi             #+#    #+#             */
/*   Updated: 2015/04/06 16:50:33 by aeddi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include <iomanip>
#include "Contact.hpp"

void		printTab(void)
{
	std::cout << std::setw(10) << std::right << "Index" << " | " <<
	std::setw(10) << std::right << "First name" << " | " <<
	std::setw(10) << std::right << "Last name" << " | " <<
	std::setw(10) << std::right << "Nickname" << " | " << std::endl;
}

int			main(void)
{
	std::string	command;
	Contact		list[8];
	int			i = 0, i2, limit = 0;
	int			choice;

	while (42)
	{
		std::cout << "phonebook > ";
		std::getline(std::cin, command, '\n');
		if (!command.compare("ADD"))
		{
			if (list[i % 8].init() == false)
				return 0;
			i++;
		}
		else if (!command.compare("SEARCH"))
		{
			limit = (i > 8) ? 8 : i;
			printTab();
			for (i2 = 0; i2 < limit; i2++)
				list[i2].display_short(i2);
			std::cout << std::endl;
			while (42 && i)
			{
				std::cout << "Choose an index number to display: ";
				std::getline(std::cin, command, '\n');
				if (std::cin.eof())
				{
					std::cout << std::endl << "Exiting program." << std::endl;
					return 0;
				}
				if (command.length() == 1 && isdigit(command[0]))
					choice = std::atoi(command.c_str());
				else
					choice = -1;
				if (choice >= 0 && choice <= 7 && choice < i)
				{
					list[choice].display_full();
					break;
				}
				else
					std::cout << "Choose a valid index number." << std::endl;
			}
		}
		else if (!command.compare("EXIT"))
			break;
		else if (std::cin.eof())
		{
			std::cout << std::endl << "Exiting program." << std::endl;
			return 0;
		}
		else
			std::cout << "Usage: enter ADD, SEARCH or EXIT" << std::endl;
	}
	return 0;
}
