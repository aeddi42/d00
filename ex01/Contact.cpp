/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Contact.cpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/05 15:14:26 by aeddi             #+#    #+#             */
/*   Updated: 2015/04/06 14:28:22 by aeddi            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include <iomanip>
#include "Contact.hpp"

std::string	truncateString(std::string str)
{
	if (str.length() > 10)
		return str.substr(0, 9) + ".";
	else
		return str;
}

Contact::Contact(void)
{
	this->eof_control = true;
}

void Contact::getprompt(std::string prompt, std::string& field)
{
	std::string entry;

	while (42 && this->eof_control)
	{
		std::cout << prompt;
		std::getline(std::cin, entry, '\n');
		if (std::cin.eof())
		{
			std::cout << std::endl << "Exiting program." << std::endl;
			this->eof_control = false;
		}
		else if (!entry.length())
			std::cout << "You must enter at least one character." << std::endl;
		else
			break;
	}
	field = entry;
	return;
}

bool Contact::init(void)
{
	std::cout << std::endl;
	this->getprompt("Enter your first name: ", this->first_n);
	this->getprompt("Enter your last name: ", this->last_n);
	this->getprompt("Enter your nickname: ", this->nick);
	this->getprompt("Enter your login: ", this->login);
	this->getprompt("Enter your postal adress: ", this->post);
	this->getprompt("Enter your email adress: ", this->email);
	this->getprompt("Enter your phone number: ", this->phone);
	this->getprompt("Enter your birthday date: ", this->birth);
	this->getprompt("Enter your favorite meal: ", this->meal);
	this->getprompt("Enter your underwear color: ", this->underw);
	this->getprompt("Enter your darkest secret: ", this->secret);
	std::cout << std::endl;
	return this->eof_control;
}

void Contact::display_short(int index)
{
	std::cout << std::setw(10) << std::right << index << " | " <<
	std::setw(10) << std::right << truncateString(this->first_n) << " | " <<
	std::setw(10) << std::right << truncateString(this->last_n) << " | " <<
	std::setw(10) << std::right << truncateString(this->nick) << " | " << std::endl;
}

void Contact::display_full(void)
{
	std::cout << std::endl;
	std::cout << "First name: " << this->first_n << std::endl;
	std::cout << "Last name: " << this->last_n << std::endl;
	std::cout << "Nickname: " << this->nick << std::endl;
	std::cout << "Login: " << this->login << std::endl;
	std::cout << "Postal adress: " << this->post << std::endl;
	std::cout << "Email adress: " << this->email << std::endl;
	std::cout << "Phone number: " << this->phone << std::endl;
	std::cout << "Birthday date: " << this->birth << std::endl;
	std::cout << "Favorite meal: " << this->meal << std::endl;
	std::cout << "Underwear color: " << this->underw << std::endl;
	std::cout << "Darkest secret: " << this->secret << std::endl;
	std::cout << std::endl;
}
